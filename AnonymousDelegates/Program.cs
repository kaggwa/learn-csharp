﻿using System;

namespace AnonymousDelegates
{
    public delegate string zoneDelegate(string price);
    class Program
    {
        public static string safeZone(string price)
        {
            Console.WriteLine("Safe zone calculated");
            return price;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the zone");
            string userInput = Console.ReadLine();
            zoneDelegate f = safeZone;
            Console.WriteLine(f(userInput));

            // Keep the console window open until a key is pressed
            Console.WriteLine("\nPress Enter Key to Continue...");
            Console.ReadLine();
        }
    }
}
